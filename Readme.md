## Mapon Testing Task

---
There are three main endpoint URLs which to check.

1) root
2) API call
3) Auth Call (authorization not implemented)

Root call demonstrates that MVC stack is
working and everything is running without an issues. Incoming request is captured and
and being processed by the Router which later on decides to which controller to pass request.
Controller later on, decides to which View pass the Request, and finally happens rendering.

Which URL is mapped to which controller action is described in the `web/config/routes.php` file, in which first part
describes a route and second part describes the `Controller` and `Action`.

Api Call demonstrates the possibility making a call onto API using cURL library, however body
parsing was not implemented, as response contains body with headers.

Auth Call demonstrates the possibility to handle a GET & POST request arguments into the Router cycle and passing finally to the
Controller.

---

#### Project set up

---

Initially, I have planned to have a .env file which would be used in the database connections and docker database setup,
but did not wanted to spent a time on that, so I have hardcoded these values.

Before starting the app, please add a hosts name into the `/etc/hosts`:

``127.0.0.1 php-docker.local``

To start up the app, simply start the docker:

``$ docker-compose up``

Open the link:

`http://php-docker.local:8081`

Database connection is set in the `web/config/db.php` file.

### Summary

---

If there would be allowance to use a Laravel framework, then main points of the task would be achieved,
but since it was required to build a system from scratch, development of the basic system took most of the time (I would say 75%).

Possible solution:

Use less extendable format, more hardcoded and less functional to achieve an expected outcome.
Also, would be an improvement to connect 3rd party packages via composer, rather than downloading them, but this would require adjustments for dependency injection.