<?php

return [
    '' => 'AuthorizationController@Index',
    'auth/submit' => 'AuthorizationController@Validate',
    'api/test' => 'ApiTesterController@Test'
];
