<?php

namespace App\Routers;

use App\Controllers\Controller;

const DEFAULT_CONTROLLER_NAMESPACE = 'App\Controllers';
const DEFAULT_VIEW_NAMESPACE = 'App\Views';

const VIEW_TEMPLATE_DIR = 'app/Templates';

class Resolver {
    // Auth/AuthController
    public static function getController(string $path, $args): ?Controller
    {
        $namespace = self::_getNamespace($path, DEFAULT_CONTROLLER_NAMESPACE);
        $className = self::_getClassName($path);

        $object = null;
        $class = $namespace . '\\' . $className;

        if (class_exists($class)) {
            $object = new $class(...$args);
        }

        return $object;
    }

    public static function getView(string $className, Array $args = [ 'data' => [] ]) {
        $namespace = self::_getNamespace($className, DEFAULT_VIEW_NAMESPACE);

        $object = null;
        $class = $namespace . '\\' . $className;

        if (class_exists($class)) {
            $object = new $class(...$args);
        }

        return $object;
    }

    protected static function _getNamespace(string $path, string $defaultNamespace): string
    {
        $pathArray = explode('\\', $path);

        // Removing the last entry of path (controller name)
        array_pop($pathArray);

        $defaultNamespaceArray = explode('/', $defaultNamespace);
        $combined = array_merge($defaultNamespaceArray, $pathArray);

        return implode('/', $combined);
    }

    protected static function _getClassName(string $path): string
    {
        $elements = explode('/', $path);

        if (is_array($elements)) {
            $className = array_pop($elements);
        } else {
            $className = $elements;
        }

        return $className;
    }

    public static function existsTemplate(string $template): bool
    {
        //TODO: Add constant value here
        return file_exists('app/Templates' . '/' . $template)
            ? true
            : false;
    }

    public static function getTemplate(string $template)
    {
        $path = false;

        if (self::existsTemplate($template)) {
            $path = VIEW_TEMPLATE_DIR . '/' . $template;
        }

        return $path;
    }
}