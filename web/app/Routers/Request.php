<?php

namespace App\Routers;

class Request
{
    protected $_uri;
    protected $_controller;
    protected $_route;
    protected $_action;
    protected $_args;
    protected $_method;

    public function __construct()
    {
        $this->_uri = $_SERVER['REQUEST_URI'];
        $this->_method = $_SERVER['REQUEST_METHOD'];
        $this->_controller = null;
        $this->_route = $this->_filterUri($this->_uri);
        $this->_action = '';
        $this->_args = $this->_parseArgs();
    }

    protected function _parseArgs()
    {
        $args = [];

        if ($this->_method === 'GET' ) {
            $args = $_GET;
        } elseif ($this->_method === 'POST') {
            $args = $_POST;
        }

        return $args;
    }

    protected function _filterUri($uri): string
    {
        $reqUrl = explode('?', $uri);
        $uriArray = explode('/', $reqUrl[0]);
        $uriArray = array_filter($uriArray);
        return implode('/', $uriArray);
    }

    public function getRoute(): string
    {
        return $this->_route;
    }

    public function getArgs()
    {
        return $this->_args;
    }
}