<?php

namespace App\Routers;

use App\Routers\Request as Request;

class Router
{
    protected $default_url;
    protected Request $_request;
    protected $_controller;

    public function __construct()
    {
        $this->_request = new Request();
        $this->default_url = getConfig('config.defaultRoutes.index');
    }

    public function route()
    {
        $errorCode = 0;
        $routeConfig = $this->_getRouteConfig($this->_request);

        if ($routeConfig) {
            $controllerName = $routeConfig['controller'];
            $actionName = $routeConfig['action'];

            $args = [
                $this->_request,
                $this
            ];

            $this->_controller = Resolver::getController($controllerName, $args);

            if (!empty($this->_controller)) {
                $this->_triggerControllerAction($actionName);
            }
        }
    }

    protected function _getRouteConfig(Request $request)
    {
        $config = false;
        $routes = getConfig('routes');
        $entry = $this->_searchRoute($routes, $request->getRoute());

        if ($entry) {
            $parts = $this->_getRouteParts($entry);

            if ($parts) {
                $config = [
                    'controller' => $parts[0],
                    'action' => $this->_getActionName($parts[1])
                ];
            }
        }

        return $config;
    }

    protected function _searchRoute($config, $url)
    {
        if (array_key_exists($url, $config) && gettype($config[$url]) === 'string') {
            return $config[$url];
        }

        throw new \Error('Route not found');
    }

    protected function _getRouteParts($entry)
    {
        $parts = explode('@', $entry);
        $result = $parts;

        if (count($parts) === 0 || count($parts) === 1) {
            $result = false;
        }

        return $result;
    }

    protected function _getActionName($action): string
    {
        //TODO: add constant value here
        return strtolower($action) . 'Action';
    }

    protected function _triggerControllerAction($action): void
    {
        if (method_exists($this->_controller, $action)) {
            $this->_controller->$action();
        } else {
            throw new \Error('action does not exist');
        }
    }

    public function redirect($url)
    {

    }
}