<?php
spl_autoload_register(function($className) {
    $dir = getcwd();
    $className = str_replace('\\', '/', $className);

    // Using lowercase app directory
    $className = str_replace('App/', 'app/', $className);

    $path = $dir . '/' . $className . '.php';
    include_once($path);
});