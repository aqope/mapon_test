<?php

/** Configuration Files */
$GLOBALS['config']['routes'] = include_once('config/routes.php');
$GLOBALS['config']['layout'] = include_once('config/layout.php');

function dd($expression = array())
{
    var_dump($expression);
    die();
}

function dump($expression = array())
{
    var_dump($expression);
}

function getConfig($arrayPath)
{
    $paths = explode('.', $arrayPath);
    $scope = $GLOBALS['config'];

    foreach($paths as $path) {
        if (isset($scope[$path])) {
            $scope = $scope[$path];
        } else {
            $scope = null;
            break;
        }
    }

    return $scope;
}