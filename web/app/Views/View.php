<?php

namespace App\Views;

class View
{
    protected $_data;

    protected $_template;

    public function __construct($data)
    {
        $this->_data = $data;
    }

    public function setTemplate(string $template)
    {
        $this->_template = $template;
    }

    public function render() {
        if (file_exists($this->_template)) {
            include_once($this->_template);
        } else {
            return false;
        }

        return true;
    }
}