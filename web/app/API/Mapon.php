<?php

namespace App\API;

require_once (dirname(__FILE__) . '/../Libs/curl/curl.php');

class Mapon
{
    const METHODS_URL = [
        'route.list' => 'route/list.json'
    ];

    const API_URL = 'https://mapon.com/api/v1/';

    protected static function _getUrl(string $key) {
        $url = '';

        if (self::METHODS_URL[$key]){
            $url = self::API_URL . self::METHODS_URL[$key];
        }

        return $url;
    }

//    protected static function _parse(string $body)
//    {
//        return $body;
//    }

    public static function getRouteList()
    {
        $client = self::_getClient();
        $response = $client->get(self::_getUrl('route.list'), [...self::_getAuth()]);
        dump($response);
        //TODO: add parser for the http body
    }

    protected static function _getAuth()
    {
        //TODO: add this key into the configuration
        return [ 'key' => '5333a9720180356462a0d9615a38f6dfff4581aa'];
    }

    protected static function _getClient()
    {
        $curl = new \Curl();
        $curl->headers['Content-Type'] = 'application/json';
        $curl->headers['Accept'] = 'application/json';
        return $curl;
    }
}