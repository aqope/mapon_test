<?php

namespace App\Controllers;

use App\Routers\Resolver;
use App\Routers\Router;
use App\Routers\Request;

class Controller
{
    /**
     * @var Router
     */
    protected Router $_router;
    /**
     * @var Request
     */
    protected Request $_request;

    public function __construct(Request $request, Router $router)
    {
        $this->_request = $request;
        $this->_router = $router;
    }

    public function render()
    {
        $template = getConfig('layout' . '.' . 'routes' . '.' . $this->_request->getRoute());
        if (!empty($template)) {

            if (Resolver::existsTemplate($template['template'])) {

                $view = Resolver::getView($template['class']);

                $view->setTemplate(Resolver::getTemplate($template['template']));
                $view->render();
            }
        }
    }
}